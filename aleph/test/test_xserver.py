import unittest
import aleph
import configparser
import os
import lxml.etree as etree

class BaseXServerTestCase(unittest.TestCase):
    def setUp(self):

        dirname = os.path.dirname(os.path.abspath(__file__))
        config = configparser.ConfigParser()
        config.read(os.path.join(dirname, 'aleph.cfg'))
        if 'credentials' in config:
            aleph_config = dict(config['credentials'])
        else:
            aleph_config = {
                'bib_base': 'duk01',
                'hol_base': 'duk60',
                'adm_library': 'duk50',
                'server': 'http://catalog.library.duke.edu/X'
            }
        self.xs = aleph.Xserver(aleph_config)

class TestXserverMethods(BaseXServerTestCase):

    def test_find_doc(self):
        record = self.xs.find_doc('10', 'bib')
        title = record.title()
        self.assertEqual(title,
            'The Da Vinci machines; tales of the population explosion.')

    def test_find_doc_int(self):
        record = self.xs.find_doc(10, 'bib')
        title = record.title()
        self.assertEqual(title,
            'The Da Vinci machines; tales of the population explosion.')


class TestXserverError(BaseXServerTestCase):
    def test_xserver_error(self):
        with self.assertRaises(Exception) as context:
            record = self.xs.find_doc('abdcefgh', 'bib')
        self.assertTrue('Error reading document' in str(context.exception))


class TestXserverItemMethods(BaseXServerTestCase):

    def test_read_item(self):
        item = self.xs.read_item(barcode='B1725225')
        material_type = item['z30-material']
        self.assertEqual('BOOK', material_type)


class TestXserverFindMethods(BaseXServerTestCase):

    def test_find(self):
        find = self.xs.find(request="sys=3829284", base=self.xs.bib_base)
        no_records = int(find.find('.//no_records').text)
        self.assertEqual(1, no_records)

    def test_find_error(self):
        with self.assertRaises(Exception) as context:
            find = self.xs.find(request="sys=abdecef", base=self.xs.bib_base)
        self.assertTrue('empty set' in str(context.exception))

    def test_find_doc_default(self):
        find = self.xs.find(request="wrd=labor laws and legislation",
            base=self.xs.bib_base)
        set_number = find.find('.//set_number').text
        present = self.xs.present(set_number)
        self.assertTrue(present.find('.//record') is not None)

    def test_find_doc_set_entry(self):
        find = self.xs.find(request="wrd=labor laws and legislation",
            base=self.xs.bib_base)
        set_number = find.find('.//set_number').text
        present = self.xs.present(set_number, set_entry='000000043')
        self.assertTrue(present.find('.//record') is not None)

if __name__ == '__main__':
    unittest.main()
