import requests
import lxml.etree as etree
import os
from io import BytesIO
import pymarc
from .errors import XserverError
from .model import Item

class Xserver:

    def __init__(self, config):
        self.config = config
        self.set_config(config)


    def set_config(self, config):
        for key, value in config.items():
            setattr(self, key, value)


    def call_xserver(self, params, error_check=True):
        '''pass a http request to the xserver. return a lxml tree'''
        if self.user_name and self.user_password:
            params['user_name'] = self.user_name
            params['user_password'] = self.user_password
        r = requests.post(self.server, data=params)
        content = etree.XML(r.content)  # bytes into lxml
        if error_check:
            self.error_check(content)
        return content


    def find_doc(self, doc_number, base):
        '''return an etree object with our document'''
        params = {'op': 'find-doc',
                  'doc_number': str(doc_number).zfill(9)}
        if base == 'bib':
            params['base'] = self.bib_base
        elif base == 'hol':
            params['base'] = self.hol_base
        content = self.call_xserver(params)
        records = self.parse_records(content)
        if len(records) > 1:
            raise IndexError("more than one record retrieved")
        return records[0] # return the first record


    def parse_records(self, xml):
        '''Parse a set of xml documents into MARC record objects.

        Return an array of MARC record objects.
        '''
        xsl = etree.XSLT(etree.XML(self._oai_marc2marcxml()))
        data = xsl(xml)
        bytes_buffer = BytesIO(etree.tostring(xsl(xml)))
        records = pymarc.parse_xml_to_array(bytes_buffer)
        bytes_buffer.close()
        return records


    def _oai_marc2marcxml(self):
        '''Get our xsl template'''
        directory = os.path.dirname(os.path.abspath(__file__))
        xsl_file = os.path.join(directory, 'oai_marc2marcxml.xsl')
        f = open(xsl_file, 'rb')
        xsl = f.read()
        f.close()
        return xsl


    def error_check(self, data, title=None, text=None):
        text = "".join(data.xpath('//h1/text()'))
        if "Error" in text:
            raise XserverError(text)
        elif data.xpath("//error"):
            msgs = data.xpath("//error/text()")
            if msgs:
                raise XserverError(msgs[0])
            else:
                raise XserverError()
        return True


    def read_item(self, doc_number=None, item_sequence=None,
            barcode=None, translate='N'):
        '''get a single item from the xserver, return the record'''
        if (doc_number is None and
                item_sequence is None) and barcode is None:
            raise ValueError("Item key must be given. Provide doc number " +
                " and item sequence or item barcode")
        params = {'op': 'read_item',
                       'doc_number': doc_number,
                       'item_sequence': item_sequence,
                       'item_barcode': barcode,
                       'library': self.adm_library,
                       'translate': translate}

        if doc_number and item_sequence:
            params['doc_number'] = params['doc_number'].zfill(9)
            params['item_sequence'] = params['item_sequence'].zfill(6)
        content = self.call_xserver(params)
        item = Item(content)
        return item


    def update_item(self, item_xml, translate='N'):
        """Update item in ALEPH"""
        params = {'op': 'update_item',
                  'library': self.adm_library,
                  'xml_full_req': item_xml,
                  'translate': translate}
        content = self.call_xserver(params, error_check=False)
        return content


    def find(self, request, base, code=None, adjacent='N'):
        """Run a CCL query on ALEPH

        This service retrieves a set number and the number of records
        answering a search request inserted by the user.
        """
        params = {'op': 'find',
                  'request': request,
                  'base': base,
                  'adjacent': adjacent }

        if code is not None:
            params['code'] = None
        content = self.call_xserver(params, error_check=True)
        return content

    def present(self, set_number, set_entry=None, format="marc",
            char_conv=None, show_sub6='N'):
        '''This service retrieves OAI XML format of expanded documents

        set_entry: 000000001-000000100 or 000000023,000000054,000000123
        '''
        params = {'op': 'present',
                  'set_number': str(set_number).zfill(6),
                  'format': format,
                  'show_sub6': show_sub6 }
        if char_conv is not None:
            params['char_conv'] = char_conv
        if set_entry is None:
            params['set_entry'] = '{0:09d}-{1:09d}'.format(1, 100)
        else:
            params['set_entry'] = set_entry
        content = self.call_xserver(params, error_check=True)
        return content
