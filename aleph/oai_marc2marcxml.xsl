<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns="http://www.loc.gov/MARC21/slim"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<collection xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xsi:schemaLocation="http://www.loc.gov/MARC21/slim
			http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd">
			<xsl:for-each select="//record">
				<record>
					<xsl:apply-templates select=".//fixfield"/>
					<xsl:apply-templates select=".//varfield"/>
				</record>
			</xsl:for-each>
		</collection>
	</xsl:template>

	<xsl:template match="fixfield">
		<xsl:choose>
			<xsl:when test="@id = 'LDR'">
				<leader><xsl:value-of select="."/></leader>
				<controlfield>
					<xsl:attribute name="tag">001</xsl:attribute>
					<xsl:choose>
					  <xsl:when test="ancestor::record/doc_number">
					    <xsl:value-of select="ancestor::record/doc_number"></xsl:value-of>
					  </xsl:when>
					  <xsl:otherwise>
					    <xsl:value-of select="following-sibling::fixfield[@id='001']"></xsl:value-of>
					  </xsl:otherwise>
					</xsl:choose>
				</controlfield>
			</xsl:when>
			<xsl:when test="@id = 'FMT'"/>

			<xsl:when test="@id = '001'"/>
			<xsl:otherwise>
				<controlfield>
					<xsl:attribute name="tag">
						<xsl:value-of select="@id"/>
					</xsl:attribute>
					<xsl:value-of select="."/>
				</controlfield>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>
	<xsl:template match="varfield">
		<datafield>
			<xsl:attribute name="tag">
				<xsl:value-of select="@id"/>
			</xsl:attribute>
			<xsl:attribute name="ind1">
				<xsl:value-of select="@i1"/>
			</xsl:attribute>
			<xsl:attribute name="ind2">
				<xsl:value-of select="@i2"/>
			</xsl:attribute>
			<xsl:apply-templates select="subfield" />
		</datafield>
	</xsl:template>
	<xsl:template match="subfield">
		<subfield>
			<xsl:attribute name="code">
				<xsl:value-of select="@label"/>
			</xsl:attribute>
			<xsl:value-of select="."/>
		</subfield>
	</xsl:template>
</xsl:stylesheet>
