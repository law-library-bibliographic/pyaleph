"""
The aleph module provides an API to access data from an ALEPH xserver.
"""
__all__ = ['xserver', 'errors']

from .xserver import Xserver
from .errors import *
from .model import *
