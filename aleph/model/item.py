import lxml.etree as etree

z30_elements = ['z30-doc-number', 'z30-item-sequence', 'z30-barcode',
                'z30-sub-library', 'z30-material', 'z30-item-status',
                'z30-open-date', 'z30-update-date', 'z30-cataloger',
                'z30-date-last-return', 'z30-hour-last-return',
                'z30-ip-last-return', 'z30-no-loans', 'z30-alpha',
                'z30-collection', 'z30-call-no-type', 'z30-call-no',
                'z30-call-no-key', 'z30-call-no-2-type', 'z30-call-no-2',
                'z30-call-no-2-key', 'z30-description', 'z30-note-opac',
                'z30-note-circulation', 'z30-note-internal',
                'z30-order-number', 'z30-inventory-number',
                'z30-inventory-number-date', 'z30-last-shelf-report-date',
                'z30-price', 'z30-shelf-report-number', 'z30-on-shelf-date',
                'z30-on-shelf-seq', 'z30-doc-number-2',
                'z30-schedule-sequence-2', 'z30-copy-sequence-2',
                'z30-vendor-code', 'z30-invoice-number', 'z30-line-number',
                'z30-pages', 'z30-issue-date', 'z30-expected-arrival-date',
                'z30-arrival-date', 'z30-item-statistic',
                'z30-item-process-status', 'z30-copy-id',
                'z30-hol-doc-number', 'z30-temp-location',
                'z30-enumeration-a', 'z30-enumeration-b', 'z30-enumeration-c',
                'z30-enumeration-d', 'z30-enumeration-e', 'z30-enumeration-f',
                'z30-enumeration-g', 'z30-enumeration-h',
                'z30-chronological-i', 'z30-chronological-j',
                'z30-chronological-k', 'z30-chronological-l',
                'z30-chronological-m', 'z30-supp-index-o', 'z30-85x-type',
                'z30-depository-id', 'z30-linking-number',
                'z30-gap-indicator', 'z30-maintenance-count',
                'z30-process-status-date']

class Item(object):
    """
    A single item.
    """
    def __init__(self, xml=None, **kwds):
        if xml is not None:
            z30 = xml.find('.//z30')
            self._xml = z30
            self.data = dict()
            for elem in self._xml:
                self.data[elem.tag] = elem.text
        else:
            self.data = kwds
            print(self.data)

    def __repr__(self):
        return("<Item {0}-{1}>".format(self['z30-doc-number'],
                                       self['z30-item-sequence']))

    def __str__(self):
        return self.__repr__()

    def __getitem__(self, key):
        return self.data[key]

    def __setitem__(self, key, value):
        self.data[key] = value
        return self.data[key]

    def to_string(self, xml_declaration=True):
        tree = etree.Element('z30')
        for key in z30_elements:
            if key in self.data:
                element = etree.SubElement(tree, key)
                element.text = self.data[key]
        return etree.tostring(tree, pretty_print=True,
                              xml_declaration=xml_declaration,
                              encoding='UTF-8')

    def tostring(self):
        return self.to_string()

    def to_tree(self):
        tree = etree.Element('z30')
        for key in z30_elements:
            if key in self.data:
                element = etree.SubElement(tree, key)
                element.text = self.data[key]
        return tree

    def items(self):
        return self.data.items()

    def keys(self):
        return self.data.keys()

    def values(self):
        return self.data.values()
