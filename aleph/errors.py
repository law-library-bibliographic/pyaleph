class AlephError(Exception):
    """Base exception class for pyaleph"""
    pass

class XserverError(AlephError):
    """Exception thrown when there is a response error from ALEPH

    message: error message response from ALEPH
    """
    def __init__(self, message):
        self.message = message

        super().__init__(message)
