from setuptools import setup, find_packages
setup(
    name = "pyaleph",
    version = "0.1.1",
    packages = find_packages(),
    package_data = {'aleph': ['*.xsl']},
    install_requires =  ['lxml>=2.0',
                         'pymarc>=2.0'],
    author = "Sean Chen",
    author_email = "schen@law.duke.edu"
)
