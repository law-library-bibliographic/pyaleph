# Aleph

Module for interacting with X-Server


## update-items
- need to use yyyymmdd for `date` fields
- can't use existing item data for update since it can't round trip data has incorrect values
    - item status
    - sublibrary
    - collection
    - dates
    - material type

## Example Item
```xml
<z30>
    <z30-doc-number>003834285</z30-doc-number>
    <z30-item-sequence>001470</z30-item-sequence>
    <z30-barcode>B1712280</z30-barcode>
    <z30-sub-library>LAW</z30-sub-library>
    <z30-material>ISSBD</z30-material>
    <z30-item-status>01</z30-item-status>
    <z30-open-date>20171117</z30-open-date>
    <z30-update-date>20180309</z30-update-date>
    <z30-cataloger>LAWCAT</z30-cataloger>
    <z30-date-last-return>00000000</z30-date-last-return>
    <z30-hour-last-return>0000</z30-hour-last-return>
    <z30-ip-last-return/>
    <z30-no-loans>000</z30-no-loans>
    <z30-alpha>L</z30-alpha>
    <z30-collection>LGEN</z30-collection>
    <z30-call-no-type/>
    <z30-call-no/>
    <z30-call-no-key/>
    <z30-call-no-2-type/>
    <z30-call-no-2/>
    <z30-call-no-2-key/>
    <z30-description>this is for kathleen</z30-description>
    <z30-note-opac/>
    <z30-note-circulation/>
    <z30-note-internal/>
    <z30-order-number>1003678</z30-order-number>
    <z30-inventory-number/>
    <z30-inventory-number-date>00000000</z30-inventory-number-date>
    <z30-last-shelf-report-date>00000000</z30-last-shelf-report-date>
    <z30-price/>
    <z30-shelf-report-number/>
    <z30-on-shelf-date>00000000</z30-on-shelf-date>
    <z30-on-shelf-seq>000000</z30-on-shelf-seq>
    <z30-doc-number-2>000000000</z30-doc-number-2>
    <z30-schedule-sequence-2>00000</z30-schedule-sequence-2>
    <z30-copy-sequence-2>00000</z30-copy-sequence-2>
    <z30-vendor-code/>
    <z30-invoice-number/>
    <z30-line-number>00000</z30-line-number>
    <z30-pages/>
    <z30-issue-date>00000000</z30-issue-date>
    <z30-expected-arrival-date>00000000</z30-expected-arrival-date>
    <z30-arrival-date>20180304</z30-arrival-date>
    <z30-item-statistic/>
    <z30-item-process-status>OO</z30-item-process-status>
    <z30-copy-id/>
    <z30-hol-doc-number>004820840</z30-hol-doc-number>
    <z30-temp-location>N</z30-temp-location>
    <z30-enumeration-a/>
    <z30-enumeration-b/>
    <z30-enumeration-c/>
    <z30-enumeration-d/>
    <z30-enumeration-e/>
    <z30-enumeration-f/>
    <z30-enumeration-g/>
    <z30-enumeration-h/>
    <z30-chronological-i/>
    <z30-chronological-j/>
    <z30-chronological-k/>
    <z30-chronological-l/>
    <z30-chronological-m/>
    <z30-supp-index-o/>
    <z30-85x-type/>
    <z30-depository-id/>
    <z30-linking-number>000000000</z30-linking-number>
    <z30-gap-indicator/>
    <z30-maintenance-count>000</z30-maintenance-count>
    <z30-process-status-date>20171117</z30-process-status-date>
    <z30-upd-time-stamp>201803091055011</z30-upd-time-stamp>
    <z30-ip-last-return-v6/>
</z30>

```
